/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::armour::{BodyPartReference, BodyPartReferenceType};
use crate::record::{Pair, RecordId, RecordPair, RecordTypeName};
use crate::util::{
    parse_float, push_opt_str_subrecord, push_str_subrecord, subrecords_len, take_nt_str,
    RecordError,
};
use derive_more::{Constructor, Display};
use esplugin::{RecordHeader, Subrecord};
use hashlink::LinkedHashMap;
use nom::number::complete::{le_f32, le_u8};
use nom_derive::Parse;
use num_traits::FromPrimitive;
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[derive(Display, Copy, Clone, Debug, Eq, PartialEq, Serialize, Deserialize, FromPrimitive)]
pub enum ClothingType {
    Pants = 0,
    Shoes = 1,
    Shirt = 2,
    Belt = 3,
    Robe = 4,
    RightGlove = 5,
    LeftGlove = 6,
    Skirt = 7,
    Ring = 8,
    Amulet = 9,
}

#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Clothing {
    /// In-game name for the Clothing
    pub name: Option<String>,
    /// Model identifier
    pub model: Option<String>,
    /// Icon identifier
    pub icon: Option<String>,
    /// Type of Clothing
    pub clothing_type: ClothingType,
    /// Weight of the Clothing
    pub weight: f64,
    /// Value of the Clothing
    pub value: i64,
    /// Enchantment Points for enchanting the armour
    pub enchantment_points: i64,
    /// FIXME: What is this used for?
    pub body_parts: LinkedHashMap<BodyPartReferenceType, BodyPartReference>,
    /// Script (FIXME: When is the script run?)
    pub script: Option<String>,
    /// Clothing's Enchantment
    pub enchant: Option<String>,
}

impl TryFrom<esplugin::Record> for RecordPair<Clothing> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut name = None;
        let mut model = None;
        let mut script = None;
        let mut icon = None;
        let mut enchant = None;
        let mut clothing_data = None;
        let mut body_parts = LinkedHashMap::new();

        #[derive(Nom)]
        #[nom(LittleEndian)]
        struct Data {
            clothing_type: i32,
            #[nom(Parse = "le_f32")]
            weight: f32,
            value: i16,
            enchant: i16,
        }

        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"MODL" => model = Some(take_nt_str(data)?.1),
                b"SCRI" => script = Some(take_nt_str(data)?.1),
                b"ITEX" => icon = Some(take_nt_str(data)?.1),
                b"ENAM" => enchant = Some(take_nt_str(data)?.1),
                b"CTDT" => {
                    clothing_data = Some(Data::parse(data)?.1);
                }
                b"DELE" => (),
                b"INDX" => {
                    let body_part = le_u8(data)?.1;
                    body_parts.insert(
                        FromPrimitive::from_u8(body_part)
                            .ok_or(RecordError::InvalidBodyPart(body_part))?,
                        BodyPartReference::new(None, None),
                    );
                }
                b"BNAM" => {
                    if let Some((key, mut value)) = body_parts.pop_back() {
                        value.male_name = Some(take_nt_str(data)?.1);
                        body_parts.insert(key, value);
                    } else {
                        return Err(RecordError::MissingSubrecord("INDX"));
                    }
                }
                b"CNAM" => {
                    if let Some((key, mut value)) = body_parts.pop_back() {
                        value.female_name = Some(take_nt_str(data)?.1);
                        body_parts.insert(key, value);
                    } else {
                        return Err(RecordError::MissingSubrecord("INDX"));
                    }
                }
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }

        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let data = clothing_data.ok_or(RecordError::MissingSubrecord("CLDT"))?;
        let clothing = Clothing::new(
            name,
            model,
            icon,
            FromPrimitive::from_i32(data.clothing_type)
                .ok_or(RecordError::InvalidClothingType(data.clothing_type))?,
            parse_float(data.weight),
            data.value as i64,
            data.enchant as i64,
            body_parts,
            script,
            enchant,
        );
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Clothing, id),
            clothing,
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Clothing> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.model, "MODL")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.icon, "ITEX")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.script, "SCRI")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.enchant, "ENAM")?;

        let mut data = vec![];
        data.extend(&(self.record.clothing_type as i32).to_le_bytes());
        data.extend(&(self.record.weight as f32).to_le_bytes());
        data.extend(&(self.record.value as i16).to_le_bytes());
        data.extend(&(self.record.enchantment_points as i16).to_le_bytes());
        subrecords.push(Subrecord::new(*b"CTDT", data, false));

        for (key, value) in self.record.body_parts {
            let subs: Vec<Subrecord> = Pair::new(key, value).try_into()?;
            subrecords.extend(subs);
        }

        let header = RecordHeader::new(*b"CLOT", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
