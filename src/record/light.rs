/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::record::{RecordId, RecordPair, RecordTypeName};
use crate::types::{take_rgba_colour, RGBAColour};
use crate::util::{
    parse_float, push_opt_str_subrecord, push_str_subrecord, subrecords_len, take_nt_str,
    RecordError,
};
use derive_more::{Constructor, Display};
use enumflags2::BitFlags;
use esplugin::{RecordHeader, Subrecord};
use hashlink::LinkedHashSet;
use nom::number::complete::{le_f32, le_i32, le_u32};
use serde::{Deserialize, Serialize};
use std::convert::{TryFrom, TryInto};

#[bitflags]
#[derive(Display, Copy, Clone, Debug, Eq, PartialEq, Hash, Serialize, Deserialize)]
#[repr(u32)]
pub enum LightTraits {
    /// FIXME: What is a dynamic light in this context?
    Dynamic = 0x001,
    /// This light can be carried
    Carry = 0x002,
    /// This light produces darkness
    Negative = 0x004,
    /// This light flickers
    Flicker = 0x008,
    /// This light is fire
    Fire = 0x010,
    /// This light does not burn when placed in a cell, but can burn when equipped
    OffDefault = 0x020,
    ///This light flickers slowly
    FlickerSlow = 0x040,
    /// This light pulses
    Pulse = 0x080,
    /// This light pulses slowly
    PulseSlow = 0x100,
}

/// Static and carryable light sources
#[skip_serializing_none]
#[derive(DeltaRecord, FullRecord, Constructor, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Light {
    /// In-game name for the light
    name: Option<String>,
    /// Model file
    model: Option<String>,
    /// Inventory icon for the light
    icon: Option<String>,
    /// FIXME: How is the script triggered?
    script: Option<String>,
    /// Identifier for the sound which emanates from the light
    sound: Option<String>,
    /// Weight of the light object
    weight: f64,
    /// Market value of the light object
    value: u32,
    /// FIXME: Is this the length of light production?
    /// FIXME: What do negative values mean?
    time: i32,
    /// Radius of the produced light
    radius: u32,
    /// Colour of the produced light
    colour: RGBAColour,
    /// Traits posessed by this light
    traits: LinkedHashSet<LightTraits>,
}

impl TryFrom<esplugin::Record> for RecordPair<Light> {
    type Error = RecordError;
    fn try_from(record: esplugin::Record) -> Result<Self, RecordError> {
        let subrecords = record.subrecords();
        let mut id = None;
        let mut model = None;
        let mut name = None;
        let mut icon = None;
        let mut script = None;
        let mut sound = None;
        let mut weight = None;
        let mut value = None;
        let mut time = None;
        let mut radius = None;
        let mut colour = None;
        let mut traits = LinkedHashSet::new();
        for subrecord in subrecords {
            let data = subrecord.data();
            match subrecord.subrecord_type() {
                b"NAME" => id = Some(take_nt_str(data)?.1),
                b"FNAM" => name = Some(take_nt_str(data)?.1),
                b"MODL" => model = Some(take_nt_str(data)?.1),
                b"ITEX" => icon = Some(take_nt_str(data)?.1),
                b"SCRI" => script = Some(take_nt_str(data)?.1),
                b"SNAM" => sound = Some(take_nt_str(data)?.1),
                b"LHDT" => {
                    let (data, _weight) = le_f32(data)?;
                    let (data, _value) = le_i32(data)?;
                    let (data, _time) = le_i32(data)?;
                    let (data, _radius) = le_i32(data)?;
                    let (data, _colour) = take_rgba_colour(data)?;
                    let (_, flags) = le_u32(data)?;
                    weight = Some(parse_float(_weight));
                    value = Some(_value as u32);
                    time = Some(_time);
                    radius = Some(_radius as u32);
                    colour = Some(_colour);
                    traits.extend(BitFlags::<LightTraits>::from_bits(flags)?.iter());
                }
                b"DELE" => (),
                x => Err(RecordError::UnexpectedSubrecord(*x))?,
            }
        }
        let id = id.ok_or(RecordError::MissingSubrecord("NAME"))?;
        let weight = weight.ok_or(RecordError::MissingSubrecord("LHDT"))?;
        let value = value.ok_or(RecordError::MissingSubrecord("LHDT"))?;
        let time = time.ok_or(RecordError::MissingSubrecord("LHDT"))?;
        let radius = radius.ok_or(RecordError::MissingSubrecord("LHDT"))?;
        let colour = colour.ok_or(RecordError::MissingSubrecord("LHDT"))?;
        Ok(RecordPair::new(
            RecordId::String(RecordTypeName::Light, id),
            Light::new(
                name, model, icon, script, sound, weight, value, time, radius, colour, traits,
            ),
        ))
    }
}

impl TryInto<esplugin::Record> for RecordPair<Light> {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Record, RecordError> {
        let mut subrecords: Vec<Subrecord> = vec![];
        let id: String = self.id.try_into()?;

        push_str_subrecord(&mut subrecords, &id, "NAME")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.name, "FNAM")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.model, "MODL")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.icon, "ITEX")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.script, "SCRI")?;
        push_opt_str_subrecord(&mut subrecords, &self.record.sound, "SNAM")?;

        let mut data = vec![];
        data.extend(&(self.record.weight as f32).to_le_bytes());
        data.extend(&(self.record.value as i32).to_le_bytes());
        data.extend(&(self.record.time as i32).to_le_bytes());
        data.extend(&(self.record.radius as i32).to_le_bytes());
        data.extend(&self.record.colour.to_le_bytes());
        let mut flags = 0u32;
        for flag in self.record.traits {
            flags |= flag as u32;
        }
        data.extend(&flags.to_le_bytes());
        subrecords.push(Subrecord::new(*b"LHDT", data, false));

        let header = RecordHeader::new(*b"LIGH", 0, None, subrecords_len(&subrecords));

        Ok(esplugin::Record::new(header, subrecords))
    }
}
