/*
 *  This file is part of DeltaPlugin.
 *
 *  Copyright (C) 2020 Benjamin Winger
 *
 *  DeltaPlugin is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DeltaPlugin is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DeltaPlugin.  If not, see <https://www.gnu.org/licenses/>.
 */

use crate::float::Float;
use crate::types::float_vector_to_bytes;
use crate::util::{byte_to_float, is_default, nt_str, take_nt_str, take_point, RecordError};
use derive_more::Display;
use enumflags2::BitFlags;
use hashlink::LinkedHashSet;
use mint::Point3;
use nom::number::complete::{le_i16, le_i32, le_i8, le_u16, le_u32, le_u64, le_u8};
use nom::sequence::tuple;
use serde::{Deserialize, Serialize};
use std::convert::TryInto;

#[bitflags]
#[derive(Display, Copy, Clone, Debug, Eq, PartialEq, Hash, Serialize, Deserialize)]
#[repr(u32)]
pub enum Services {
    /// This merchant buys:
    Weapon = 0x00001,
    Armor = 0x00002,
    Clothing = 0x00004,
    Books = 0x00008,
    Ingredients = 0x00010,
    Picks = 0x00020,
    Probes = 0x00040,
    Lights = 0x00080,
    Apparatus = 0x00100,
    RepairItem = 0x00200,
    Misc = 0x00400,
    Potions = 0x02000,

    // AllItems = Weapon|Armor|Clothing|Books|Ingredients|Picks|Probes|Lights|Apparatus|RepairItem|Misc|Potions,
    /// Other services
    Spells = 0x00800,
    MagicItems = 0x01000,
    Training = 0x04000,
    Spellmaking = 0x08000,
    Enchanting = 0x10000,
    Repair = 0x20000,
    Unknown1 = 0x40000,
    Unknown2 = 0x80000,
    Unknown3 = 0x100000,
    Unknown4 = 0x200000,
    Unknown5 = 0x400000,
    Unknown6 = 0x800000,
    Unknown7 = 0x1000000,
}

// TODO: Make deltas treat this as part of the structure that wraps it
#[derive(DeltaRecord, Clone, Debug, Default, PartialEq, Serialize, Deserialize)]
pub struct AIData {
    pub greeting_distance: u32,
    pub fight_chance: f64,
    pub flee_chance: f64,
    pub alarm_chance: f64,
    unknown: [i8; 3],
    #[serde(default, skip_serializing_if = "is_default")]
    pub services: LinkedHashSet<Services>,
}

impl AIData {
    pub fn parse(data: &[u8]) -> Result<Self, RecordError> {
        let (_, (greeting_distance, fight_chance, flee_chance, alarm_chance, u1, u2, u3, services)) =
            tuple((le_u16, le_u8, le_u8, le_u8, le_i8, le_i8, le_i8, le_u32))(data)?;

        Ok(AIData {
            greeting_distance: greeting_distance as u32,
            fight_chance: byte_to_float(fight_chance),
            flee_chance: byte_to_float(flee_chance),
            alarm_chance: byte_to_float(alarm_chance),
            unknown: [u1, u2, u3],
            services: BitFlags::<Services>::from_bits(services)?.iter().collect(),
        })
    }
}

impl TryInto<esplugin::Subrecord> for AIData {
    type Error = RecordError;
    fn try_into(self) -> Result<esplugin::Subrecord, RecordError> {
        let mut data = vec![];
        data.extend(&(self.greeting_distance as u16).to_le_bytes());
        data.push((self.fight_chance * 255.0).round() as u8);
        data.push((self.flee_chance * 255.0).round() as u8);
        data.push((self.alarm_chance * 255.0).round() as u8);
        data.extend(self.unknown.iter().map(|x| *x as u8));
        let mut flagbits = 0;
        for flag in self.services {
            flagbits |= flag as u32;
        }
        data.extend(&(flagbits as u32).to_le_bytes());
        Ok(esplugin::Subrecord::new(*b"AIDT", data, false))
    }
}

#[skip_serializing_none]
#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub struct AITarget {
    /// FIXME: Location of target?
    pub target: Point3<Float>,
    /// FIXME: ID of target?
    pub id: String,
    /// FIXME: Duration of what?
    pub duration: i32,
    unknown: i16,
    pub cell: Option<String>,
}

#[derive(Clone, Debug, PartialEq, Eq, Hash, Serialize, Deserialize)]
pub enum Behaviour {
    /// FIXME: Describe
    Wander {
        distance: u64,
        duration: u64,
        time_of_day: u64,
        idle: [u8; 8],
        should_repeat: bool,
    },
    /// FIXME: Describe
    Travel {
        /// FIXME: Is this correct?
        direction: Point3<Float>,
        unknown: i32,
    },
    /// FIXME: Describe
    Escort(AITarget),
    Follow(AITarget),
    /// FIXME: Describe
    Activate {
        name: String,
        unknown: u8,
    },
}

impl Behaviour {
    pub fn parse(subrecord_type: &[u8; 4], data: &[u8]) -> Result<Self, RecordError> {
        match subrecord_type {
            b"AI_W" => {
                let (_, (distance, duration, time_of_day, idle, should_repeat)) =
                    tuple((le_i16, le_i16, le_u8, le_u64, le_u8))(data)?;
                Ok(Behaviour::Wander {
                    distance: distance as u64,
                    duration: duration as u64,
                    time_of_day: time_of_day as u64,
                    idle: idle.to_le_bytes(),
                    should_repeat: should_repeat != 0,
                })
            }
            b"AI_T" => {
                let (_, (point, unknown)) = tuple((take_point, le_i32))(data)?;
                Ok(Behaviour::Travel {
                    direction: point,
                    unknown,
                })
            }
            b"AI_F" | b"AI_E" => {
                let (data, target) = take_point(data)?;
                let (data, duration) = le_i16(data)?;
                let (_, id) = take_nt_str(&data[..32])?;
                let (_, unknown) = le_i16(&data[32..])?;

                let target = AITarget {
                    target,
                    id,
                    duration: duration as i32,
                    unknown,
                    cell: None,
                };
                match subrecord_type {
                    b"AI_F" => Ok(Behaviour::Follow(target)),
                    b"AI_E" => Ok(Behaviour::Escort(target)),
                    _ => unreachable!(),
                }
            }
            b"AI_A" => {
                let (_, name) = take_nt_str(&data[..32])?;
                let (_, unknown) = le_u8(&data[32..])?;
                Ok(Behaviour::Activate { name, unknown })
            }
            x => Err(RecordError::UnexpectedSubrecord(*x))?,
        }
    }
}

impl TryInto<Vec<esplugin::Subrecord>> for Behaviour {
    type Error = RecordError;
    fn try_into(self) -> Result<Vec<esplugin::Subrecord>, RecordError> {
        use self::Behaviour::*;
        let is_escort = matches!(&self, Escort(_));
        match self {
            Wander {
                distance,
                duration,
                time_of_day,
                idle,
                should_repeat,
            } => {
                let mut data = vec![];
                data.extend(&(distance as i16).to_le_bytes());
                data.extend(&(duration as i16).to_le_bytes());
                data.extend(&(time_of_day as u8).to_le_bytes());
                for i in idle.iter() {
                    data.push(*i as u8);
                }
                data.extend(&(should_repeat as u8).to_le_bytes());
                Ok(vec![esplugin::Subrecord::new(*b"AI_W", data, false)])
            }
            Travel { direction, unknown } => {
                let mut data = vec![];
                data.extend(float_vector_to_bytes(direction));
                data.extend(&(unknown as i32).to_le_bytes());
                Ok(vec![esplugin::Subrecord::new(*b"AI_T", data, false)])
            }
            Escort(target) | Follow(target) => {
                let mut data = vec![];
                data.extend(float_vector_to_bytes(target.target));
                data.extend(&(target.duration as i16).to_le_bytes());
                let mut id_bytes = nt_str(&target.id)?;
                id_bytes.resize(32, 0);
                data.extend(id_bytes);
                data.extend(&(target.unknown as i16).to_le_bytes());
                let mut result = vec![];
                if is_escort {
                    result.push(esplugin::Subrecord::new(*b"AI_E", data, false));
                } else {
                    result.push(esplugin::Subrecord::new(*b"AI_F", data, false));
                }
                if let Some(cell) = target.cell {
                    result.push(esplugin::Subrecord::new(*b"CNDT", nt_str(&cell)?, false));
                }
                Ok(result)
            }
            Activate { name, unknown } => {
                let mut data = vec![];
                data.extend(nt_str(&name)?);
                data.resize(32, 0);
                data.push(unknown as u8);
                Ok(vec![esplugin::Subrecord::new(*b"AI_A", data, false)])
            }
        }
    }
}
