# DeltaPlugin

[![docs](https://img.shields.io/badge/docs-master-blue)](https://bmwinger.gitlab.io/delta-plugin/delta_plugin/index.html)
[![minimum rustc 1.49](https://img.shields.io/badge/rustc-1.49+-blue.svg)](https://rust-lang.github.io/rfcs/2495-min-rust-version.html)

Transcoder for managing a form of markup-based minimal Elder Scrolls Plugin files
and OMWAddon files

Unlike their binary counterparts, these files provide the option to modify records
as little or as much as necessary, rather than being forced to replicate information
from overridden records just to modify a single subrecord, or part of a subrecord.

To achieve this, all records are restructured into a more flat format (no multi-value
data subrecords), and have a corresponding "Delta" record which allows all fields except
for the identifier to be omitted, and provides mechanisms for modifying lists and mappings
without replacing the entire list or mapping.

Currently supports translation from Morrowind-like plugin files (esm, esp, omwaddon, omwgame, etc.) to yaml and back again, including automatic detection of differences between records and their masters.

Also supports the creation of a merged plugin using the load order as defined in `openmw.cfg` by creating such delta plugins for each plugin in `openmw.cfg`, combining them (in order), and storing the records that were changed in a new file.

## Building

Requires rust 1.49 or newer.

## Usage

```
delta_plugin 0.15.0
Benjamin Winger <bmw@disroot.org>
Transcoder for managing a form of markup-based minimal Elder Scrolls Plugin files and OMWAddon files

USAGE:
    delta_plugin [FLAGS] [OPTIONS] [SUBCOMMAND]

FLAGS:
    -h, --help       Prints help information
    -q, --quiet      Run in quiet mode
    -V, --version    Prints version information
    -v, --verbose    Sets the level of verbosity, which is higher the more times this argument is repeated.

OPTIONS:
    -c, --openmw-cfg <path>    Path to openmw.cfg

SUBCOMMANDS:
    apply      Applies a patch. The source and destination file should be relative to the current directory.
    convert    Converts files from esp to yaml-encoded Delta and vice versa
    deps       Produces a newline separated list of plugin dependencies. Unlike other commands, this supports all
               esp formats.
    diff       Diffs two plugin files and creates a unified text diff representing the difference between the two
               files
    help       Prints this message or the help of the given subcommand(s)
    merge      Creates a merged plugin for the current plugin configuration as defined in openmw.cfg
```

If the file doesn't contain scripts, the `convert` output will be in a yaml file of the same base name as the original plugin.

If the plugin contains scripts, `convert` will produce a subdirectory called `FILE.d`, which contains `FILE.yaml`, and a `scripts` directory containing a `.mwscript` file for each script. If you use the `--inline` argument, the scripts will instead be inlined into the yaml file, however note that espmarkup currently only includes newlines in strings as `\n` escape characters.

DeltaPlugin also supports creating patches for esp/omwaddon files by producing the unified diff of the canonical yaml forms of the plugins (i.e. as produced by the serializer, without extra comments or formatting). While it's possible that it could produce patches in the form of Delta records, certain records (Cell and Dialogue) are treated in an unusual manner by the engine, so the method used to create deltas for these records are not useable for creating patches which apply directly to the file.
